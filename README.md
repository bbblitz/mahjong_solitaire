# Mahjong Solitaire

The first tech demo of a homebrew game engine, [Brok\[en\]gine](https://cogarr.net/source/cgit.cgi/brokengine/).

## Download

Click the appropriate download above for your system. There is no OSX version, since I don't have a mac. (I'd love if someone could help me with that though.).

## How to play

Standard mahjong solitaire, remove pairs of matching tiles until there are none left. Tiles can be removed if they can slide out to the left or right without disturbing other tiles.

## Authors

Code by [Alexander Pickering](cogarr.net)
Art by [Code Inferno](www.codeinferno.com)

## Legal Notice

DO NOT REMOVE THIS DISCLAIMER

For optimum performance and safety, please read these instructions carefully.

Void where prohibited. No representation or warranty, express or implied, with respect to completeness, accuracy, fitness for a particular purpose, or utility of these materials or any information or opinion contained herein. This product is not to be construed as an endorsement of any product or company, nor as the adoption or promulgation of any guidelines, standards or recommendations. Any resemblance to actual people, living or dead, or events, past, present or future, is purely coincidental. All models over 18 of age. Batteries not included. Prices slightly higher west of the Mississippi. Do not eat.
